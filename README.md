## CONFIG

1. npm init -y
2. npm i 
if starting over from scratch then : 
3. npm i --save-dev `typescript`
4. npm i --save-dev `ts lint`
5. npm i --save-dev `@types/node` `@types/express`
6. npm i `express` 
7. npm i `dotenv`
8. npm i --save-dev @types/dotenv

finally
9. rename `.env_sample` to `.env`


## PROJECT DESCRIPTION

`index.ts` --- core typescript file that will get compiled into javascript.

`tsconfig.json` --- settings to compile files, where to get files (`src/index.ts`), and where to store results (`dist` folder).

`dist` --- folder containing the output of the compiled files, the created build.

`ts lint` --- instructs how to lint the code. Analyzes potential problems code will have in production other than syntax issues.

`package.json` --- the main is set to the `dist` `indes.js` as it is what will be shown. the scripts have a `prebuild` in order to lint the code and check the config set in `tsconfig.json` before compiling, the `build` creates our main file, the `pres-start` runs it first as a primilinary check, and then the `start` command officially starts everything. the `test` will echo back some errors. 

`dotenv` --- setups configuration for opening server, choosing the `port` and allows to separate dev and prod later on more cleanly
